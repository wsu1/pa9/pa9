#include "Header.h"
#include "Game.h"

void Game::loadGameWindow(const string& status)
{
    if (status == "GameMenu")
        window.create(VideoMode(500, 300), "Game Menu");
    else if (status == "PlayGame")
        window.create(VideoMode(460, 450), "Tic-Tac-Toe");
    else if (status == "GameTutorial")
        window.create(VideoMode(640, 426), "Tic-Tac-Toe");
}

bool Game::loadPieceFiles()
{
    #if defined linux || defined __linux__ || defined __APPLE__ // Inspired from here: https://stackoverflow.com/a/6649992
    return (gameBoard.loadFromFile("../images/board.png") &&
            pieceX.loadFromFile("../images/x"
                                ".png") && pieceO.loadFromFile("../images/o.png"));
    #endif

    #if defined OS_Windows || defined _WIN32 // Inspired from here: https://stackoverflow.com/a/6649992
        return (gameBoard.loadFromFile("..\pa9\images\board.png") &&
                pieceX.loadFromFile("..\pa9\images\x.png") && pieceO.loadFromFile("..\pa9\images\o.png"));
    #endif
}

void Game::setGamePieces()
{
    /* Overloaded Sprites */
    addBoard.setTexture(gameBoard);
    addX.setTexture(pieceX);
    addO.setTexture(pieceO);
}

bool Game::loadGameOver()
{
    #if defined linux || defined __linux__ || defined __APPLE__ // Inspired from here: https://stackoverflow.com/a/6649992
        return (gameOver.loadFromFile("../images/GameOver.png"));
    #endif

    #if defined OS_Windows || defined _WIN32 // Inspired from here: https://stackoverflow.com/a/6649992
        return (gameOver.loadFromFile("..\pa9\images\GameOver.png"));
    #endif
}

void Game::setGameOver()
{
    /* Overloaded Sprites */
    addBoard.setTexture(gameOver);
}

bool Game::loadGameMenu()
{
#if defined linux || defined __linux__ || defined __APPLE__ // Inspired from here: https://stackoverflow.com/a/6649992
    return (menu.loadFromFile("../images/menu.png"));
#endif

#if defined OS_Windows || defined _WIN32 // Inspired from here: https://stackoverflow.com/a/6649992
    return (menu.loadFromFile("..\pa9\images\menu.png"));
#endif
}

void Game::setGameMenu()
{
    /* Overloaded Sprites */
    addBoard.setTexture(menu);
}

bool Game::loadGameTutorial()
{
#if defined linux || defined __linux__ || defined __APPLE__ // Inspired from here: https://stackoverflow.com/a/6649992
    return (tutorial.loadFromFile("../images/Tutorial.png"));
#endif

#if defined OS_Windows || defined _WIN32 // Inspired from here: https://stackoverflow.com/a/6649992
    return (tutorial.loadFromFile("..\pa9\images\Tutorial.png"));
#endif
}

void Game::setGameTutorial()
{
    /* Overloaded */
    addBoard.setTexture(tutorial);
}

void Game::setUserPrompt()
{
    message.setString("Player 1's turn");
    message.setCharacterSize(40);
    message.move(90, 310);
}

void Game::updateUserPrompt(int turn, bool invalidPos)
{
    std::string playerPrompt;
    if(invalidPos){
        playerPrompt = "Position already\n     taken!";
    } else {
        playerPrompt = turn % 2 == 0? "Player 1's turn" : "Player 2's turn";
    }

    message.setString(playerPrompt);

}

void Game::setRectCoordinates(IntRect &rect, int rectLeft, int rectTop, int rectWidth, int rectHeight)
{
    rect.left = rectLeft;
    rect.top = rectTop;
    rect.width = rectWidth;
    rect.height = rectHeight;
}

void Game::rectangleCoordinates()
{
    setRectCoordinates(grid[0], 0, 0, 120, 120);
    setRectCoordinates(grid[1], 130, 0, 262, 120);
    setRectCoordinates(grid[2], 280, 0, 400, 120);

    setRectCoordinates(grid[3], 0, 133, 113, 270);
    setRectCoordinates(grid[4], 130, 133, 262, 270);
    setRectCoordinates(grid[5], 280, 133, 400, 270);

    setRectCoordinates(grid[6], 0, 283, 113, 400);
    setRectCoordinates(grid[7], 130, 283, 262, 400);
    setRectCoordinates(grid[8], 280, 283, 400, 400);
}

void Game::setPositionPieces(Vector2i &pos, int left, int top)
{
    pos.x = left;
    pos.y = top;

}

void Game::positionPiece()
{
    setPositionPieces(position[0], 15, 20);
    setPositionPieces(position[1], 170, 20);
    setPositionPieces(position[2], 310, 20);

    setPositionPieces(position[3], 15, 166);
    setPositionPieces(position[4], 170, 166);
    setPositionPieces(position[5], 310, 166);

    setPositionPieces(position[6], 15, 316);
    setPositionPieces(position[7], 170, 316);
    setPositionPieces(position[8], 310, 316);


}

bool Game::isClickInBounds(int boardPos)
{
    return event.mouseButton.x >= grid[boardPos].left
           && event.mouseButton.x <= grid[boardPos].width
           && event.mouseButton.y >= grid[boardPos].top
           && event.mouseButton.y <= grid[boardPos].height;

}

bool Game::takeTurn(int player, char board[3][3])
{
    for(int boardPosition = 0; boardPosition < 9; boardPosition++)
    {
        if(isClickInBounds(boardPosition))
        {
            if(grid[boardPosition].left != -500)
            {
                piece[player] = player % 2 == 0? addX : addO;
                cout << "X Position: " << (float)position[boardPosition].x << endl;
                cout << "Y Position: " << (float)position[boardPosition].y << endl;
                /* Fill gameBoard array */
                fillBoard(player, position[boardPosition].x, position[boardPosition].y, board);

                piece[player].move((float)position[boardPosition].x, (float)position[boardPosition].y);
                grid[boardPosition].left = -500;
                updateUserPrompt(player, false);
                return true;
            }
            else
            {
                updateUserPrompt(player, true);
                return false;
            }
        }
    }
    return false;
}

bool Game::gameOverScreen(const string& status)
{
    window.create(VideoMode(400, 200), status);
    window.clear(Color(50, 50, 50));
    if (!loadGameOver())
        return false;
    setGameOver();
    while (window.isOpen()) {
        while (window.pollEvent(event)) {
            if (event.type == Event::Closed) {
                window.close();
            }
            if (Keyboard::isKeyPressed(Keyboard::Num2) || Keyboard::isKeyPressed(Keyboard::Numpad2)) {
                window.close();
            }
            if (Keyboard::isKeyPressed(Keyboard::Num1) || Keyboard::isKeyPressed(Keyboard::Numpad1)) {
                repeat = true;
                window.close();
            }
        }
        window.draw(addBoard);
        window.display();
    }

    return true;
}

void Game::resetGame()
{
    for (int i = 0; i < 9; i++)
        /* Inspired from here: https://en.sfml-dev.org/forums/index.php?topic=17997.0 */
        piece[i].setColor(Color::Transparent);
}