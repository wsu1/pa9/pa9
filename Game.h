/*********************************************************************
Programmer: Keegan Milsten, Ethan Lee, and Captionrex (As per Discord :) )
Class: CptS122, Spring, 2020; Lab Section 09
Programming Assignment: PA 9
Date: April 26, 2020
Description: This program allows you to play Tic Tac Toe
			 against another player - all in a user-friendly GUI!
*********************************************************************/
#ifndef PA_9_GAME_H
#define PA_9_GAME_H

/* Much of the Game class below is inspired from here: https://codereview.stackexchange.com/q/27679 */
class Game : private TicTacToe {
private:
    Texture gameBoard, pieceX, pieceO, gameOver, menu, tutorial, large;
    Sprite addX, addO;
    IntRect grid[9];
    Vector2i position[9];
public:
    /*******************************************************************************
    * Function: loadGameWindow
    * Date Created: 4/27/2020
    * Date Last Modified: 5/1/2020
    * Description: Creates a window based on the inputted string.
    * Input parameters: A string
    * Returns: Nothing
    * Preconditions: Existance of a string
    * Postconditions: A window is created
    ********************************************************************************/
    void loadGameWindow(const string &);
    /*******************************************************************************
    * Function: loadPieceFiles
    * Date Created: 4/27/2020
    * Date Last Modified: 5/1/2020
    * Description: Loads the png files for the window.
    * Input parameters: None
    * Returns: status of operation
    * Preconditions: A created window
    * Postconditions: png files are now loaded
    ********************************************************************************/
    bool loadPieceFiles();
    /*******************************************************************************
    * Function: setGamePieces
    * Date Created: 4/27/2020
    * Date Last Modified: 5/1/2020
    * Description: Sets the board as well as x and y sprites according to the window's needs.
    * Input parameters: None
    * Returns: Nothing
    * Preconditions: A created window
    * Postconditions: Sprites are assigned accordingly.
    ********************************************************************************/
    void setGamePieces();
    /*******************************************************************************
    * Function: setUserPrompt
    * Date Created: 4/27/2020
    * Date Last Modified: 4/27/2020
    * Description: Displays the current user's turn.
    * Input parameters: None
    * Returns: User
    * Preconditions: An active game of Tic-Tac-Toe
    * Postconditions: User displayed
    ********************************************************************************/
    void setUserPrompt();
    /*******************************************************************************
    * Function: rectangleCoordinates
    * Date Created: 4/27/2020
    * Date Last Modified: 4/27/2020
    * Description: Sets the coordinates for the rectangles in Tic-Tac-Toe.
    * Input parameters: None
    * Returns: Nothing
    * Preconditions: A created window
    * Postconditions: Rectangle Coordinates set
    ********************************************************************************/
    void rectangleCoordinates();
    /*******************************************************************************
    * Function: setRectCoordinates
    * Date Created: 4/27/2020
    * Date Last Modified: 4/27/2020
    * Description: Sets the corner coordinates for the rectangles in Tic-Tac-Toe.
    * Input parameters: None
    * Returns: Nothing
    * Preconditions: A created window
    * Postconditions: Rectangle corner coordinates set
    ********************************************************************************/
    static void setRectCoordinates(IntRect &rect, int, int, int, int);
    /*******************************************************************************
    * Function: setPositionPieces
    * Date Created: 4/27/2020
    * Date Last Modified: 4/27/2020
    * Description: Sets the corner coordinates for the X's and O's in Tic-Tac-Toe.
    * Input parameters: position, left and top coordinates
    * Returns: Nothing
    * Preconditions: A created window
    * Postconditions: X and O corner coordinates set
    ********************************************************************************/
    static void setPositionPieces(Vector2i &pos, int, int);
    /*******************************************************************************
    * Function: positionPiece
    * Date Created: 4/27/2020
    * Date Last Modified: 4/27/2020
    * Description: Sets the coordinates for the X's and O's in Tic-Tac-Toe.
    * Input parameters: None
    * Returns: Nothing
    * Preconditions: A created window
    * Postconditions: X and O coordinates set
    ********************************************************************************/
    void positionPiece();
    /*******************************************************************************
    * Function: isClickInBounds
    * Date Created: 4/27/2020
    * Date Last Modified: 4/27/2020
    * Description: Detects if a click during Tic-Tac-Toe is inside a square.
    * Input parameters: boardPos
    * Returns: Nothing
    * Preconditions: A created window with set coordinates
    * Postconditions: returns if the click was in-bounds
    ********************************************************************************/
    bool isClickInBounds(int);
    /*******************************************************************************
    * Function: takeTurn
    * Date Created: 4/27/2020
    * Date Last Modified: 4/29/2020
    * Description: Process's a user's turn based on their click.
    * Input parameters: player, board array (used with the backend code - through inheritance!
    * Returns: Next turn or not
    * Preconditions: A created window
    * Postconditions: Player's move has been processed
    ********************************************************************************/
    bool takeTurn(int, char[3][3]);
    /*******************************************************************************
    * Function: updateUserPrompt
    * Date Created: 4/27/2020
    * Date Last Modified: 4/27/2020
    * Description: Updates player text
    * Input parameters: turn, invalidPos
    * Returns: Nothing
    * Preconditions: A created window
    * Postconditions: Player text has been updated
    ********************************************************************************/
    void updateUserPrompt(int, bool);
    /*******************************************************************************
    * Function: loadGameOver
    * Date Created: 4/29/2020
    * Date Last Modified: 5/1/2020
    * Description: Loads the png files for the window.
    * Input parameters: None
    * Returns: status of operation
    * Preconditions: A created window
    * Postconditions: png files are now loaded
    ********************************************************************************/
    bool loadGameOver();
    /*******************************************************************************
    * Function: setGameOver
    * Date Created: 4/29/2020
    * Date Last Modified: 5/1/2020
    * Description: Sets the board as well as x and y sprites according to the window's needs.
    * Input parameters: None
    * Returns: Nothing
    * Preconditions: A created window
    * Postconditions: Sprites are assigned accordingly.
    ********************************************************************************/
    void setGameOver();
    /*******************************************************************************
    * Function: gameOverScreen
    * Date Created: 4/28/2020
    * Date Last Modified: 5/1/2020
    * Description: Offers the user options upon completing a game of Tic-Tac-Toe.
    * Input parameters: None
    * Returns: Whether the window ran into issues or not.
    * Preconditions: A created window
    * Postconditions: User input is now logged
    ********************************************************************************/
    bool gameOverScreen(const string &);
    /*******************************************************************************
    * Function: setGameMenu
    * Date Created: 4/29/2020
    * Date Last Modified: 5/1/2020
    * Description: Sets the board as well as x and y sprites according to the window's needs.
    * Input parameters: None
    * Returns: Nothing
    * Preconditions: A created window
    * Postconditions: Sprites are assigned accordingly.
    ********************************************************************************/
    void setGameMenu();
    /*******************************************************************************
    * Function: loadGameMenu
    * Date Created: 4/29/2020
    * Date Last Modified: 5/1/2020
    * Description: Loads the png files for the window.
    * Input parameters: None
    * Returns: status of operation
    * Preconditions: A created window
    * Postconditions: png files are now loaded
    ********************************************************************************/
    bool loadGameMenu();
    /*******************************************************************************
    * Function: resetGame
    * Date Created: 4/30/2020
    * Date Last Modified: 5/1/2020
    * Description: Resets the board sprite so that the player can repeat the game
    *              if they so desire.
    * Input parameters: None
    * Returns: Nothing
    * Preconditions: addBoard sprite exists
    * Postconditions: addBoard sprite is reset
    ********************************************************************************/
    void resetGame();
    /*******************************************************************************
    * Function: loadGameTutorial
    * Date Created: 4/30/2020
    * Date Last Modified: 5/1/2020
    * Description: Loads the png files for the window.
    * Input parameters: None
    * Returns: status of operation
    * Preconditions: A created window
    * Postconditions: png files are now loaded
    ********************************************************************************/
    bool loadGameTutorial();
    /*******************************************************************************
    * Function: setGameTutorial
    * Date Created: 4/27/2020
    * Date Last Modified: 5/1/2020
    * Description: Sets the board as well as x and y sprites according to the window's needs.
    * Input parameters: None
    * Returns: Nothing
    * Preconditions: A created window
    * Postconditions: Sprites are assigned accordingly.
    ********************************************************************************/
    void setGameTutorial();

    RenderWindow window;
    Sprite addBoard;
    Text message;
    Sprite piece[9];
    Event event;

    bool repeat = true;
};

#endif //PA_9_GAME_H
