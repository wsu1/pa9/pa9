/*********************************************************************
Programmer: Keegan Milsten, Ethan Lee, and Captionrex (As per Discord :) )
Class: CptS122, Spring, 2020; Lab Section 09
Programming Assignment: PA 9
Date: April 26, 2020
Description: This program allows you to play Tic Tac Toe
			 against another player - all in a user-friendly GUI!
*********************************************************************/
#define _CRT_SECURE_NO_WARNINGS

#ifndef PA_9_TicTacToe_H
#define PA_9_TicTacToe_H
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <limits>
#include <ctime>
#include <cctype>
#include<string>
/* Include the SFML library for the GUI */
#include <SFML/Graphics.hpp>

using namespace sf;

#include <iostream>

using namespace std;

class TicTacToe
{
private:
    char board[3][3] = { ' ' };
public:
    static int won(char[3][3]);
    /*******************************************************************************
    * Function: fullBoard
    * Date Created: 2/20/2020
    * Date Last Modified: 4/20/2020
    * Description: Determines whether or not the board is completely full
    * Input parameters: Board array
    * Returns: True or false value
    * Preconditions: Existence of board array
    * Postconditions: None
    ********************************************************************************/
    static int fullBoard(char[3][3]);
    /*******************************************************************************
    * Function: clearBoard
    * Date Created: 4/28/2020
    * Date Last Modified: 4/29/2020
    * Description: Clears the board so that the player is not marked as having won
    *              before even starting.
    * Input parameters: char array of board
    * Returns: Nothing
    * Preconditions: Existance of board array
    * Postconditions: Board array is cleared
    ********************************************************************************/
    static void clearBoard(char[3][3]);
    /*******************************************************************************
    * Function: fillBoard
    * Date Created: 4/28/2020
    * Date Last Modified: 4/29/2020
    * Description: Fills the index of the board array that corresponds to user input.
    * Input parameters: player, x coordinate, y coordinate, char array of board
    * Returns: Nothing
    * Preconditions: Existance of board array, as well as x and y coordinates
    * Postconditions: Value is inputted into board arrray.
    ********************************************************************************/
    static void fillBoard(int, int, int, char[3][3]);
};

#endif