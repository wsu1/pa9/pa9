#!/usr/bin/env bash

# Run the following in your Clion terminal to use: bash ./Linux/run.sh

# If we are in the Linux directory, cd back one directory to correctly compile the program
if [[ $(pwd | grep "Linux" | wc -l) -gt 0 ]]; then
  cd ${pwd} ..
fi

# Declare variables
source="Main.cpp TicTacToe.cpp Header.h"
source2="Main.o TicTacToe.o"
delete="TicTacToe.o Header.h.gch Main.o tic-tac-toe"

# Compile and run the program using g++
g++ -c $source
g++ $source2 -o tic-tac-toe -lsfml-graphics -lsfml-window -lsfml-system
./tic-tac-toe

# Delete temporary files from the build
rm -f $delete