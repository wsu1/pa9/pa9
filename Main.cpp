#include "Header.h"
#include "Game.h"

int main()
{
    TicTacToe backend;
    Game game;
    /* Declare necessary variables */
    char board[3][3] = { 'N' };
    int turn = 0;
    string screen;

    while (game.repeat)
    {
        screen = "GameMenu";
        game.repeat = false;
        game.loadGameWindow(screen);
        game.window.clear(Color(50, 50, 50));
        if (!game.loadGameMenu())
            return false;
        game.setGameMenu();
        /* Tic Tac Toe Menu */
        while (game.window.isOpen()) {
            while (game.window.pollEvent(game.event)) {
                /* If 3 has been pressed */
                if ((game.event.type == Event::Closed) ||
                    (Keyboard::isKeyPressed(Keyboard::Num3) || Keyboard::isKeyPressed(Keyboard::Numpad3))) {
                    game.window.close();
                }
                /* If 2 has been pressed */
                if (Keyboard::isKeyPressed(Keyboard::Num2) || Keyboard::isKeyPressed(Keyboard::Numpad2)) {
                    screen = "PlayGame";
                    game.window.close();
                }
                /* If 1 has been pressed */
                if (Keyboard::isKeyPressed(Keyboard::Num1) || Keyboard::isKeyPressed(Keyboard::Numpad1)) {
                    screen = "GameTutorial";
                    game.window.close();
                }
                game.window.draw(game.addBoard);
                game.window.display();
            }
        }
        /* Display the game instructions */
        if (screen == "GameTutorial")
        {
            game.loadGameWindow(screen);
            game.window.clear(Color(50, 50, 50));
            if (!game.loadGameTutorial())
                return false;
            game.setGameTutorial();
            /* Tutorial */
            while (game.window.isOpen()) {
                while (game.window.pollEvent(game.event)) {
                    /* If Escape has been pressed */
                    if ((game.event.type == Event::Closed) ||
                        (Keyboard::isKeyPressed(Keyboard::Escape))) {
                        game.window.close();
                    }
                    game.window.draw(game.addBoard);
                    game.window.display();
                }
            }
            /* Return to the main menu */
            game.repeat = true;
        }

        /* Tic Tac Toe Game */
        if (screen == "PlayGame") {
            /* Clear the board in case this is not the first time we are playing Tic Tac Toe */
            backend.clearBoard(board);
            turn = 0;
            /* Reset x and o values */
            game.resetGame();
            game.loadGameWindow(screen);
            /* If the images for the game were not loaded correctly, return an error */
            if (!game.loadPieceFiles())
                return 1;

            game.setGamePieces();
            game.setUserPrompt();
            game.rectangleCoordinates();
            game.positionPiece();

            while (game.window.isOpen()) {
                while (game.window.pollEvent(game.event)) {
                    if (game.event.type == Event::Closed) {
                        game.window.close();
                    }
                    /* If the left mouse button has been pressed, check whether or not it is in a valid
                     * area, and then take a turn - immediately after checking if the game has been won.*/
                    if (Mouse::isButtonPressed(Mouse::Left) && !backend.fullBoard(board)) {
                        if (game.takeTurn(turn, board)) {
                            for (int i = 0; i < 3; i++) {
                                cout << board[i][0] << ", " << board[i][1] << ", " << board[i][2] << endl;
                            }
                            if (backend.won(board)) {
                                screen = "GameOver";
                                if (!game.gameOverScreen("The game is won!")) {
                                    return 1;
                                }
                            }
                            turn++;
                        }
                    }
                    /* If the left mouse button has been pressed, check whether or not the board is full,
                     * in order to determine if a draw has occurred. */
                    if (Mouse::isButtonPressed(Mouse::Left) && backend.fullBoard(board)) {
                        screen = "GameOver";
                        if (!game.gameOverScreen("Looks like it's a draw."))
                            return 1;
                    }
                }
                game.window.clear(Color(50, 50, 50));
                game.window.draw(game.addBoard);
                game.window.draw(game.message);
                for (int pieceId = 0; pieceId < 9; pieceId++) {
                    game.window.draw(game.piece[pieceId]);
                }
                game.window.display();
            }
        }
    }

    return 0;
}