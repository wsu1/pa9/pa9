#include "Header.h"

int TicTacToe::won(char board[3][3])
{
    bool player1Won = false, player2Won = false;
    /* If a player has their marks on a complete row or column, recognize the fact that they have won */
    for (int i = 0; i < 3; i++)
    {
        if (board[i][0] == 'O' && board[i][1] == 'O' && board[i][2] == 'O')
            player1Won = true;
        else if (board[i][0] == 'X' && board[i][1] == 'X' && board[i][2] == 'X')
            player2Won = true;
        else if (board[0][i] == 'O' && board[1][i] == 'O' && board[2][i] == 'O')
            player1Won = true;
        else if (board[0][i] == 'X' && board[1][i] == 'X' && board[2][i] == 'X')
            player2Won = true;
    }
    if (board[0][0] == 'O' && board[1][1] == 'O' && board[2][2] == 'O')
        player1Won = true;
    else if (board[0][0] == 'X' && board[1][1] == 'X' && board[2][2] == 'X')
        player2Won = true;
    else if (board[2][0] == 'O' && board[1][1] == 'O' && board[0][2] == 'O')
        player1Won = true;
    else if (board[2][0] == 'X' && board[1][1] == 'X' && board[0][2] == 'X')
        player2Won = true;

    if (player1Won)
        return true;
    else if (player2Won)
        return true;
    else
        return false;
}

int TicTacToe::fullBoard(char gameBoard[3][3])
{
	bool full = true;
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if (gameBoard[i][j] < 0x41)
				full = false;
		}
	}
	return full;
}

void TicTacToe::clearBoard(char gameBoard[3][3])
{
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
			gameBoard[i][j] = ' ';
	}
}

void TicTacToe::fillBoard(int player, int x, int y, char gameBoard[3][3])
{
    /* Use x and y coordinates for each square to determine which square
     * now has a value in it. */
    if (x == 15 && y == 20)
    {
        if (player % 2 == 0)
            gameBoard[0][0] = 'X';
        else
            gameBoard[0][0] = 'O';
    }
    else if (x == 170 && y == 20)
    {
        if (player % 2 == 0)
            gameBoard[0][1] = 'X';
        else
            gameBoard[0][1] = 'O';
    }
    else if (x == 310 && y == 20)
    {
        if (player % 2 == 0)
            gameBoard[0][2] = 'X';
        else
            gameBoard[0][2] = 'O';
    }
    else if (x == 15 && y == 166)
    {
        if (player % 2 == 0)
            gameBoard[1][0] = 'X';
        else
            gameBoard[1][0] = 'O';
    }
    else if (x == 170 && y == 166)
    {
        if (player % 2 == 0)
            gameBoard[1][1] = 'X';
        else
            gameBoard[1][1] = 'O';
    }
    else if (x == 310 && y == 166)
    {
        if (player % 2 == 0)
            gameBoard[1][2] = 'X';
        else
            gameBoard[1][2] = 'O';
    }
    else if (x == 15 && y == 316)
    {
        if (player % 2 == 0)
            gameBoard[2][0] = 'X';
        else
            gameBoard[2][0] = 'O';
    }
    else if (x == 170 && y == 316)
    {
        if (player % 2 == 0)
            gameBoard[2][1] = 'X';
        else
            gameBoard[2][1] = 'O';
    }
    else if (x == 310 && y == 316)
    {
        if (player % 2 == 0)
            gameBoard[2][2] = 'X';
        else
            gameBoard[2][2] = 'O';
    }
}